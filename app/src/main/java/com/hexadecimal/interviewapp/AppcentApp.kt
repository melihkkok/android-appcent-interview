package com.hexadecimal.interviewapp

import com.hexadecimal.interviewapp.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class AppcentApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out AppcentApp> {
        return DaggerAppComponent.builder().application(this).build()
    }

}