package com.hexadecimal.interviewapp.base.binding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.hexadecimal.interviewapp.R
import com.hexadecimal.interviewapp.util.ImageLoaderUtiliy

@BindingAdapter("url")
fun loadImage(
    view: ImageView,
    imageUrl: String
) {
    ImageLoaderUtiliy.imageLoaderWithCacheAndPlaceHolder(
        view.context,
        imageUrl,
        view,
        R.drawable.ic_image_place_holder
    )
}

