package com.hexadecimal.interviewapp.base.viewmodel

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(
    private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    val baseLiveData: MutableLiveData<State> = MutableLiveData()
) : ViewModel() {
    open fun handleIntent(extras: Bundle) {}
    fun getCompositeDisposable() = compositeDisposable


    override fun onCleared() {
        compositeDisposable.clear()
    }

    sealed class State {
        data class OnError(val throwable: Throwable) : State()
    }

}