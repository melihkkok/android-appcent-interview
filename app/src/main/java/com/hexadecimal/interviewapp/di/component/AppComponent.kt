package com.hexadecimal.interviewapp.di.component

import android.app.Application
import com.hexadecimal.interviewapp.AppcentApp
import com.hexadecimal.interviewapp.di.module.ActivityModule
import com.hexadecimal.interviewapp.di.module.AppModule
import com.hexadecimal.interviewapp.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        NetworkModule::class,
        ActivityModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<AppcentApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: Application)
}