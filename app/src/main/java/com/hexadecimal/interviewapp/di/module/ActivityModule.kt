package com.hexadecimal.interviewapp.di.module

import com.hexadecimal.interviewapp.ui.image_detail.ImageDetailActivity
import com.hexadecimal.interviewapp.ui.image_list.ImageListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindImageListActivity(): ImageListActivity


    @ContributesAndroidInjector
    abstract fun bindImageDetailActivity(): ImageDetailActivity

}