package com.hexadecimal.interviewapp.di.module

import dagger.Module

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {


}