package com.hexadecimal.interviewapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hexadecimal.interviewapp.di.ViewModelFactory
import com.hexadecimal.interviewapp.di.key.ViewModelKey
import com.hexadecimal.interviewapp.ui.image_detail.ImageDetailViewModel
import com.hexadecimal.interviewapp.ui.image_list.ImageListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ImageListViewModel::class)
    abstract fun bindsImageListViewModel(viewModel: ImageListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImageDetailViewModel::class)
    abstract fun bindsImageDetailViewModel(viewModel: ImageDetailViewModel): ViewModel

}