package com.hexadecimal.interviewapp.network

import com.hexadecimal.interviewapp.BuildConfig
import com.hexadecimal.interviewapp.network.response.GetRecentsResponse
import com.hexadecimal.interviewapp.network.service.ApiService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiDataManager @Inject constructor(private val apiService: ApiService) {

    fun getRecentPhotos(page: Int): Single<GetRecentsResponse> {
        return apiService.getRecentImages(
            method = "flickr.photos.getRecent",
            api_key = BuildConfig.API_KEY,
            page = page,
            per_page = 20
        )
    }
}