package com.hexadecimal.interviewapp.network.response

import com.google.gson.annotations.SerializedName

data class GetRecentsResponse(
    @SerializedName("photos")
    var photos: PhotosResponse? = null,
    @SerializedName("stat")
    var stat: String? = null
)