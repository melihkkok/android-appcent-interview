package com.hexadecimal.interviewapp.network.response

import com.google.gson.annotations.SerializedName

data class PhotosResponse(
    @SerializedName("page")
    var page: Int? = null,
    @SerializedName("pages")
    var pages: Int? = null,
    @SerializedName("perpage")
    var perpage: Int? = null,
    @SerializedName("total")
    var total: String? = null,
    @SerializedName("photo")
    var photo: List<PhotoDetailResponse>? = null
)