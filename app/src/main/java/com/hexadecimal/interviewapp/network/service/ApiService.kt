package com.hexadecimal.interviewapp.network.service

import com.hexadecimal.interviewapp.network.response.GetRecentsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("services/rest/")
    fun getRecentImages(
        @Query("method") method: String,
        @Query("api_key") api_key: String,
        @Query("per_page") per_page: Int,
        @Query("page") page: Int,
        @Query("format") format: String = "json",
        @Query("nojsoncallback") jsonNo: Int = 1
    ): Single<GetRecentsResponse>

}