package com.hexadecimal.interviewapp.ui.image_detail

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.hexadecimal.interviewapp.R
import com.hexadecimal.interviewapp.base.view.BaseActivity
import com.hexadecimal.interviewapp.databinding.ActivityImageDetailBinding
import com.hexadecimal.interviewapp.util.AppUtils

class ImageDetailActivity (
    override val layoutResourceId: Int = R.layout.activity_image_detail,
    override val classTypeOfViewModel: Class<ImageDetailViewModel> = ImageDetailViewModel::class.java
) : BaseActivity<ActivityImageDetailBinding, ImageDetailViewModel>(), View.OnClickListener {

    override fun initialize() {
        super.initialize()
        binding.viewModel = viewModel

        binding.activityDetailShareImageView.setOnClickListener(this)
        binding.activityDetailBackImageView.setOnClickListener(this)
    }

    companion object {
        fun start(activity: Activity, title: String, imageUrl: String) {
            activity.startActivity(Intent(activity, ImageDetailActivity::class.java).apply {
                putExtra("title", title)
                putExtra("imageUrl", imageUrl)
            })
        }
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.activity_detail_share_image_view -> {
                if (!viewModel.imageUrl.isNullOrEmpty()){
                    AppUtils.shareToOutApps(this, viewModel.imageUrl ?: "")
                }
            }
            R.id.activity_detail_back_image_view -> {
                onBackPressed()
            }
        }
    }
}
