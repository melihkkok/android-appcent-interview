package com.hexadecimal.interviewapp.ui.image_detail

import android.os.Bundle
import com.hexadecimal.interviewapp.base.viewmodel.BaseViewModel
import javax.inject.Inject

class ImageDetailViewModel @Inject constructor(): BaseViewModel() {

    var title: String? = ""
    var imageUrl: String? = ""

    override fun handleIntent(extras: Bundle) {
        title = extras.getString("title")
        imageUrl = extras.getString("imageUrl")
    }
}