package com.hexadecimal.interviewapp.ui.image_list

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import com.hexadecimal.interviewapp.R
import com.hexadecimal.interviewapp.base.view.BaseActivity
import com.hexadecimal.interviewapp.databinding.ActivityImageListBinding
import com.hexadecimal.interviewapp.ui.image_detail.ImageDetailActivity
import com.hexadecimal.interviewapp.ui.image_list.adapter.ImageListAdapter
import com.hexadecimal.interviewapp.util.extension.observe
import com.hexadecimal.interviewapp.util.extension.onSwipe

class ImageListActivity(
    override val layoutResourceId: Int = R.layout.activity_image_list,
    override val classTypeOfViewModel: Class<ImageListViewModel> = ImageListViewModel::class.java
) : BaseActivity<ActivityImageListBinding, ImageListViewModel>() {

    private lateinit var adapter: ImageListAdapter

    override fun initialize() {
        super.initialize()
        adapter = ImageListAdapter()
        binding.recyclerView.adapter = adapter
        binding.activityMainSwipeRefresh onSwipe {
            checkLoadingState(true)
            observe(viewModel.getItemPageList()) {
                adapter.submitList(it)
            }
        }

        observe(adapter.liveData, ::adapterStateChanged)
        observe(viewModel.liveData,::onStateChanged)
    }

    override fun initStartRequest() {
        initAdapter()
    }

    private fun onStateChanged(state:ImageListViewModel.State){
        when(state){
            is ImageListViewModel.State.OnLoadingState -> checkLoadingState(state.status)
        }
    }

    private fun adapterStateChanged(state: ImageListAdapter.AdapterState) {
        when (state) {
            is ImageListAdapter.AdapterState.onImageClick -> {
                //state.photoDetailModel
                ImageDetailActivity.start(
                    activity = this,
                    title = state.photoDetailModel.title ?: "",
                    imageUrl = state.photoDetailModel.imageUrl ?: ""
                )
            }
        }
    }

    private fun initAdapter() {
        observe(viewModel.getItemPageList()) {
            adapter.submitList(it)
        }
    }

    private fun checkLoadingState(status: Boolean){
        binding.progressbar.isVisible = status
    }

    companion object {
        fun start(activity: Activity) {
            activity.startActivity(Intent(activity, ImageListActivity::class.java))
        }
    }
}