package com.hexadecimal.interviewapp.ui.image_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.hexadecimal.interviewapp.base.viewmodel.BaseViewModel
import com.hexadecimal.interviewapp.network.ApiDataManager
import com.hexadecimal.interviewapp.ui.image_list.data_source.DataSourceFactory
import com.hexadecimal.interviewapp.ui.image_list.data_source.ItemDataSource
import com.hexadecimal.interviewapp.ui.image_list.model.PhotoDetailModel
import javax.inject.Inject

class ImageListViewModel @Inject constructor(val apiDataManager: ApiDataManager) : BaseViewModel() {

    var dataSource: ItemDataSource? = null
    var liveData = MutableLiveData<State>()

    fun getItemPageList(): LiveData<PagedList<PhotoDetailModel>> {
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(20)
            .setPrefetchDistance(5)
            .setEnablePlaceholders(true)
            .build()
        return DataSourceFactory(config, this, dataSource).builder().build()
    }

    sealed class State{
        data class OnLoadingState(var status: Boolean): State()
    }
}