package com.hexadecimal.interviewapp.ui.image_list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.hexadecimal.interviewapp.R
import com.hexadecimal.interviewapp.base.adapter.BaseViewHolder
import com.hexadecimal.interviewapp.databinding.CellImageItemBinding
import com.hexadecimal.interviewapp.ui.image_list.model.PhotoDetailModel
import com.hexadecimal.interviewapp.ui.image_list.view_holder.ImageListViewHolder

class ImageListAdapter (): PagedListAdapter<PhotoDetailModel, BaseViewHolder<*>>(diffCallback) {
    val liveData = MutableLiveData<AdapterState>()

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<PhotoDetailModel>() {
            override fun areItemsTheSame(oldItem: PhotoDetailModel, newItem: PhotoDetailModel): Boolean =
                oldItem.title == newItem.title && oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: PhotoDetailModel, newItem: PhotoDetailModel): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ItemType.values()[viewType].onCreateViewHolder(parent, layoutInflater, liveData)
    }

    override fun getItemViewType(position: Int): Int {
        return ItemType.PHOTO.viewType()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is ImageListViewHolder -> holder.bind(getItem(position) ?: PhotoDetailModel())
        }
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    enum class ItemType {
        PHOTO {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<AdapterState>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellImageItemBinding>(
                    layoutInflater,
                    R.layout.cell_image_item,
                    parent,
                    false
                )
                return ImageListViewHolder(binding, liveData)
            }
        };

        abstract fun onCreateViewHolder(
            parent: ViewGroup,
            layoutInflater: LayoutInflater,
            liveData: MutableLiveData<AdapterState>
        ): BaseViewHolder<*>

        fun viewType(): Int = ordinal
    }

    sealed class AdapterState {
        data class onImageClick(val photoDetailModel: PhotoDetailModel) : AdapterState()
    }
}