package com.hexadecimal.interviewapp.ui.image_list.data_source

import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.hexadecimal.interviewapp.ui.image_list.ImageListViewModel
import com.hexadecimal.interviewapp.ui.image_list.model.PhotoDetailModel

class DataSourceFactory(
    val config: PagedList.Config,
    val viewModel: ImageListViewModel,
    var dataSource: ItemDataSource?
) {

    fun builder(): LivePagedListBuilder<Int, PhotoDetailModel> {
        val dataSourceFactory = object : DataSource.Factory<Int, PhotoDetailModel>() {
            override fun create(): DataSource<Int, PhotoDetailModel> {
                dataSource = ItemDataSource(viewModel)
                return dataSource!!
            }
        }
        return LivePagedListBuilder<Int, PhotoDetailModel>(dataSourceFactory, config)
    }
}