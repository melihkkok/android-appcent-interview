package com.hexadecimal.interviewapp.ui.image_list.data_source

import androidx.paging.PageKeyedDataSource
import com.hexadecimal.interviewapp.base.viewmodel.BaseViewModel
import com.hexadecimal.interviewapp.ui.image_list.ImageListViewModel
import com.hexadecimal.interviewapp.ui.image_list.model.PhotoDetailModel
import com.hexadecimal.interviewapp.util.extension.toPhotoDetailModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ItemDataSource(private val viewModel: ImageListViewModel) :
    PageKeyedDataSource<Int, PhotoDetailModel>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, PhotoDetailModel>
    ) {
        val manage = viewModel.apiDataManager.getRecentPhotos(page = 0)
        viewModel.getCompositeDisposable().add(
            manage.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewModel.liveData.value = ImageListViewModel.State.OnLoadingState(true)
                    val nextPage =
                        if (it.photos?.pages ?: 0 > it.photos?.page?.plus(1) ?: 0) it.photos?.page?.plus(1) else null

                    it.photos?.photo?.let { list ->
                        callback.onResult(
                            list.map { photoDetail -> photoDetail.toPhotoDetailModel() },
                            null,
                            nextPage
                        )
                        viewModel.liveData.value = ImageListViewModel.State.OnLoadingState(false)

                    }

                }, {
                    viewModel.liveData.value = ImageListViewModel.State.OnLoadingState(false)
                    viewModel.baseLiveData.value = BaseViewModel.State.OnError(it)
                })
        )
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, PhotoDetailModel>
    ) {
        val manage = viewModel.apiDataManager.getRecentPhotos(page = params.key)
        viewModel.getCompositeDisposable().add(
            manage.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val nextPage =
                        if (it.photos?.pages ?: 0 > it.photos?.page?.plus(1) ?: 0) it.photos?.page?.plus(
                            1
                        ) else null

                    it.photos?.photo?.let { list ->
                        callback.onResult(
                            list.map { photoDetail -> photoDetail.toPhotoDetailModel() },
                            nextPage
                        )
                    }
                }, {
                    it.printStackTrace()
                })
        )
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, PhotoDetailModel>
    ) {
    }

}