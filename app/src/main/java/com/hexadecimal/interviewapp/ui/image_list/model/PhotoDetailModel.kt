package com.hexadecimal.interviewapp.ui.image_list.model

class PhotoDetailModel constructor(
    val id: String? = "",
    val owner: String? = "",
    val secret: String? = "",
    val server: String? = "",
    val farm: Int? = 0,
    val title: String? = "",
    val imageUrl: String? = ""
)