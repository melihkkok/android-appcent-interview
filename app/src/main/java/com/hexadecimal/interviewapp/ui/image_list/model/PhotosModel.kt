package com.hexadecimal.interviewapp.ui.image_list.model

class PhotosModel constructor(
    val page: Int? = 0,
    val pages: Int? = 0,
    val perPage: Int? = 0,
    val total: String? = "",
    val photo: List<PhotoDetailModel>? = emptyList()
)