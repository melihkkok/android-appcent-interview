package com.hexadecimal.interviewapp.ui.image_list.view_holder

import androidx.lifecycle.MutableLiveData
import com.hexadecimal.interviewapp.base.adapter.BaseViewHolder
import com.hexadecimal.interviewapp.databinding.CellImageItemBinding
import com.hexadecimal.interviewapp.ui.image_list.adapter.ImageListAdapter
import com.hexadecimal.interviewapp.ui.image_list.model.PhotoDetailModel


class ImageListViewHolder(
    val binding: CellImageItemBinding,
    private val liveData: MutableLiveData<ImageListAdapter.AdapterState>
) : BaseViewHolder<PhotoDetailModel>(binding.root) {
    override fun bind(data: PhotoDetailModel) {
        binding.photo = data

        binding.cellItemTopConstraint.setOnClickListener {
            liveData.value =
                ImageListAdapter.AdapterState.onImageClick(data)
        }
    }

}