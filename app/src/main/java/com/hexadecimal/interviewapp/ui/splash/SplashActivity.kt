package com.hexadecimal.interviewapp.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.hexadecimal.interviewapp.R
import com.hexadecimal.interviewapp.ui.image_list.ImageListActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        goToImageListActivity()
    }

    private fun goToImageListActivity() {
        val handler = Handler()
        handler.postDelayed({
            ImageListActivity.start(this)
        }, 3000)
    }
}