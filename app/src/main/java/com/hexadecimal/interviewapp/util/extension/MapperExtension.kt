package com.hexadecimal.interviewapp.util.extension

import com.hexadecimal.interviewapp.network.response.PhotoDetailResponse
import com.hexadecimal.interviewapp.network.response.PhotosResponse
import com.hexadecimal.interviewapp.ui.image_list.model.PhotoDetailModel
import com.hexadecimal.interviewapp.ui.image_list.model.PhotosModel

fun PhotosResponse.toPhotosModel() = PhotosModel(
    page = this.page ?: 0,
    pages = this.pages ?: 0,
    perPage = this.perpage ?: 20,
    photo = this.photo?.map { it.toPhotoDetailModel() } ?: emptyList()
)

fun PhotoDetailResponse.toPhotoDetailModel() = PhotoDetailModel(
    secret = this.secret ?: "",
    server = this.server ?: "",
    farm = this.farm ?: 0,
    title = this.title ?: "",
    imageUrl = "https://farm"
            + farm +
            ".staticflickr.com/"
            + server +
            "/"
            + id +
            "_"
            + secret +
            ".jpg"
)