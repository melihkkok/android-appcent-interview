package com.hexadecimal.interviewapp.util.extension

import android.view.View
import androidx.core.view.isVisible
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

inline infix fun SwipeRefreshLayout.onSwipe(crossinline block: () -> Unit) {
    this.setOnRefreshListener {
        block()
        isRefreshing = false
    }
}

infix fun View.setVisibility(visibilityFlag: Boolean){
    this.isVisible = visibilityFlag
}